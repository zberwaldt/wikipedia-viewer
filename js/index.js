var search = document.querySelector("#wikiSearch");
var searchBtn = document.querySelector("#search-btn").addEventListener('click', function (e) {
  var searchItm = search.value;
  $.ajax({
    type: "POST",
    url: "https://en.wikipedia.org/w/api.php?action=opensearch&format=json&origin=*&search=" + searchItm,
    async: false,
    dataType: 'json',
    success: function success(data) {
      var titles = data[1];
      var descriptions = data[2];
      var links = data[3];
      for (var i = 0; i < 10; i++) {
        var result = document.createElement('a');
        result.setAttribute("href", links[i]);
        result.setAttribute("class", "wikiResult");
        result.setAttribute("target", "_blank");
        var wrapper = document.createElement('div');
        wrapper.setAttribute("class", "wrapper");
        var resultTitle = document.createElement('h3');
        var titleContent = document.createTextNode(titles[i]);
        resultTitle.appendChild(titleContent);
        var resultDesc = document.createElement('p');
        var descContent = document.createTextNode(descriptions[i]);
        resultDesc.appendChild(descContent);
        wrapper.appendChild(resultTitle);
        wrapper.appendChild(resultDesc);
        result.appendChild(wrapper);
        console.log(result);
        document.querySelector('.results').appendChild(result);
      }
    },
    error: function error(errorMessage) {
      console.log(errorMessage);
    }
  });
  e.preventDefault();
}, false);